from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    try:
        # Create a dictionary for the headers to use in the request
        # Create the URL for the request with the city and state
        url = "https://api.pexels.com/v1/search"
        headers = {"Authorization": PEXELS_API_KEY}
        params = {"query": city + " " + state, "per_page": 1}

        # Make the request
        response = requests.get(url, params=params, headers=headers)
        # Parse the JSON response
        unencoded = json.loads(response.content)

        url = unencoded["photos"][0]["url"]
        # Return a dictionary that contains a `picture_url` key and
        #   one of the URLs for one of the pictures in the response
        return {"picture_url": url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    try:
        # Get the latitude and longitude from the response
        lat = json.loads(response.content)[0]["lat"]
        lon = json.loads(response.content)[0]["lon"]
    except IndexError:
        return None

    # Create the URL for the current weather API with the lat/long
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    try:
        # Get the main temperature and the weather's description and put
        #   them in a dictionary
        description = json.loads(response.content)["weather"][0]["description"]
        temp = json.loads(response.content)["main"]["temp"]
        weather = {"description": description, "temp": temp}
    except IndexError:
        return None
    # Return the dictionary
    return weather
